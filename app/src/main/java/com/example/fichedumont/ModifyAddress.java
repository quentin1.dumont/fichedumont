package com.example.fichedumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ModifyAddress extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_address);
        Intent intent = getIntent();
        EditText numero = findViewById(R.id.numero);
        EditText rue = findViewById(R.id.rue);
        EditText codePostal = findViewById(R.id.code_postal);
        EditText ville = findViewById(R.id.ville);
        numero.setText(intent.getStringExtra("numero"));
        rue.setText(intent.getStringExtra("rue"));
        codePostal.setText(intent.getStringExtra("codePostal"));
        ville.setText(intent.getStringExtra("ville"));
    }

    public void valider(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        EditText numero = findViewById(R.id.numero);
        EditText rue = findViewById(R.id.rue);
        EditText codePostal = findViewById(R.id.code_postal);
        EditText ville = findViewById(R.id.ville);
        intent.putExtra("numero", numero.getText().toString());
        intent.putExtra("rue", rue.getText().toString());
        intent.putExtra("codePostal", codePostal.getText().toString());
        intent.putExtra("ville", ville.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}