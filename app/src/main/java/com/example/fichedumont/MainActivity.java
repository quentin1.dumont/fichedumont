package com.example.fichedumont;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void modifierIdentite(View view)
    {
        Intent intent = new Intent(this, ModifyIdentity.class);
        TextView nom = findViewById(R.id.nom);
        TextView prenom = findViewById(R.id.prenom);
        TextView numero = findViewById(R.id.numero);
        intent.putExtra("nom", nom.getText().toString());
        intent.putExtra("prenom", prenom.getText().toString());
        intent.putExtra("numero", numero.getText().toString());
        startActivityForResult(intent, 1);
    }

    public void modifierAdresse (View view)
    {
        Intent intent = new Intent(this, ModifyAddress.class);
        TextView numero = findViewById(R.id.numero_rue);
        TextView rue = findViewById(R.id.rue);
        TextView codePostal = findViewById(R.id.code_postal);
        TextView ville = findViewById(R.id.ville);
        intent.putExtra("numero", numero.getText().toString());
        intent.putExtra("rue", rue.getText().toString());
        intent.putExtra("codePostal", codePostal.getText().toString());
        intent.putExtra("ville", ville.getText().toString());
        startActivityForResult(intent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode==1) && (resultCode==RESULT_OK))
        {
            TextView nom = findViewById(R.id.nom);
            TextView prenom = findViewById(R.id.prenom);
            TextView numero = findViewById(R.id.numero);
            nom.setText(data.getStringExtra("nom"));
            prenom.setText(data.getStringExtra("prenom"));
            numero.setText(data.getStringExtra("numero"));
        } else if ((requestCode==2) && (resultCode==RESULT_OK)) {
            TextView numero = findViewById(R.id.numero_rue);
            TextView rue = findViewById(R.id.rue);
            TextView codePostal = findViewById(R.id.code_postal);
            TextView ville = findViewById(R.id.ville);
            numero.setText(data.getStringExtra("numero"));
            rue.setText(data.getStringExtra("rue"));
            codePostal.setText(data.getStringExtra("codePostal"));
            ville.setText(data.getStringExtra("ville"));
        }
    }
}