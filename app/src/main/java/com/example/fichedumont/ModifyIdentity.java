package com.example.fichedumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ModifyIdentity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_identity);
        Intent intent = getIntent();
        EditText nom = findViewById(R.id.nom);
        EditText prenom = findViewById(R.id.prenom);
        EditText numero = findViewById(R.id.numero);
        nom.setText(intent.getStringExtra("nom"));
        prenom.setText(intent.getStringExtra("prenom"));
        numero.setText(intent.getStringExtra("numero"));
    }

    public void valider(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        EditText nom = findViewById(R.id.nom);
        EditText prenom = findViewById(R.id.prenom);
        EditText numero = findViewById(R.id.numero);
        intent.putExtra("nom", nom.getText().toString());
        intent.putExtra("prenom", prenom.getText().toString());
        intent.putExtra("numero", numero.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}